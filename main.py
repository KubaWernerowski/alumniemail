#!/usr/bin/env python3

# Standard library imports
import csv
import time
import os
import glob     # listing file names.
from typing import Tuple, List
# Package imports
from Webpage import Webpage
from Person import Person

"""
CSV INDEX FORMAT:
0: DONOR_ID
1: First Name
2: Last Name
3: Email
4: Student_Number
5: Grad_Year
6: Program_of_Study
7: preferred_username
8: date_submitted
9: Misc. Notes
"""

username = input("Enter your iModule email: ")
password = input("Enter your iModule password: ")
PAYLOAD = {'username': username, 'password': password}


#TODO: Find a way to integrate this more nicely.
def is_invalid_grad_year(grad_year:str) -> bool:
    return (grad_year.isdigit() and int(grad_year) >= 2012) or len(grad_year) < 4 


if __name__ == "__main__":
    parent_directory = os.path.dirname(os.path.realpath("main.py"))
    all_csv_files = [os.path.basename(f) for f in glob.glob(os.path.join(parent_directory, '*.csv'))]

    print("The files you can input are:")
    for file in all_csv_files:
        print(file)

    print("-"*58)
    print("| Type the .csv file you want this script to run on.|")
    print("-"*58)

    IN_FILE = input()
    time_start = time.time()
    OUTPUT_FILE = f'./output/{IN_FILE.split(".csv")[0]}_filtered.csv' 

    print(f'Processing file: {IN_FILE}')
    print('. '* 10)

    # Initial web page setup
    iModule = Webpage()
    iModule.login(PAYLOAD)
    iModule.get_admin_page()

    iModule.driver.execute_script('window.open("");')                       # opens new tab.
    iModule.driver.switch_to_window(iModule.driver.window_handles[1])       # Focus on new tab
    iModule.get_student_num_page()

    iModule.driver.execute_script('window.open("");')   

    # For statistic purposes.
    auto_filled = 0
    total_rows = sum(1 for line in open(IN_FILE))

    with open(IN_FILE, 'r', newline='') as csvfile:
        reader = csv.reader(csvfile)        

        with open(OUTPUT_FILE, 'w', newline='') as output:
            writer = csv.writer(output)

            for row in reader:
                # In accordance with CSV Index Format.
                first, last =  row[1], row[2]
                excel_email, student_num = row[3], row[4]
                grad_year, pref_user = row[5], row[7]
                
                user = Person(first, last, excel_email, student_num, grad_year, pref_user)

                if is_invalid_grad_year(user.grad_year):
                    row[0] = "NOT ELIGBLE"
                    writer.writerow(row)
                    auto_filled += 1
                else:
                    if iModule.main_validation_process(user, row):
                        auto_filled += 1
                    
                    writer.writerow(row)

    iModule.driver.quit()

    time_delta = time.time() - time_start
    print("Statistics:")
    print(f"Script took: {round(time_delta,2)} seconds, or roughly {round(time_delta / (total_rows), 2)} seconds per request.")
    print(f"Autofilled: {round((auto_filled / total_rows) * 100, 2)}%")

