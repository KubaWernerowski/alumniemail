
## Before You Continue...
- Make sure you're doing this on a Linux Virtual Machine. This was built and tested only on Ubuntu 18.04.1 64 bit, but should work on other versions.
- To get the virtual machine running, I used this guide: http://www.psychocats.net/ubuntu/virtualbox.
- When creating the VM, allocate roughly 25 GB of space and 1/4 of your available RAM and CPU. This will allow the script to run faster and has 
  a lower chance of causing the VM to freeze.

## Prerequisites
1) You're on your Ubuntu 64 bit VM.

2) You're connected to the internet.

3) You have a working iModule account that can edit other people's accounts.

## Installation and Setup  
1) Open the terminal (CRTL + ALT + T)
2) Ubuntu 18.04.1 comes with Python 3.6.5 by default, so we only need to install pip3. Refer to https://askubuntu.com/questions/778052/installing-pip3-for-python3-on-ubuntu-16-04-lts-using-a-proxy for more details if you're having issues.

```
sudo apt-get -y install python3-pip
sudo apt-get update
```
3) If you don't already have Git on your VM, install it like this:

```
sudo apt-get install git-core
```
4) Move to the directory you want this script to be in, then run the following command, and follow any instructions.

```
git clone ENTER_URL_HERE
```
5) We want to install our dependencies, so we need to move into the cloned repo.

```
cd alumniemail/
pip3 install -r requirements.txt
```
6) We need a specific version of Geckdriver for Firefox to be used by Selenium. There is an active bug open for a process this script uses in versions past Version 0.20.1
   to get this version, we have to run several commands.
   
   
```
wget https://github.com/mozilla/geckodriver/releases/download/v0.18.0/geckodriver-v0.18.0-linux64.tar.gz

tar -xvzf geckodriver*

chmod +x geckodriver

sudo mv geckodriver /usr/local/bin/
```

## Running the Script
1) cd into the directory with the script.

2) Make sure your desired .csv file (excel) is in the same directory as the script.

3) Run the following command:

```
python3 main.py
```
4) Follow the instructions while entering your iModule login info.

5) A list of .csv files in the directory will be listed. Type in which one you want the script to run on.
   *NOTE*: If you have already run the script on this .csv file, then it will overwrite the previous output file.
   
6) Sit back and relax!


## FAQ

**The code threw an error.**

Complete error-catching of the entire codebase has yet to be finished. 

Rerun the script again and it should work just fine. If it doesn't, run it again.

If it's catching some error consistently, consider checking the geckodriver logs in trace mode (https://firefox-source-docs.mozilla.org/testing/geckodriver/geckodriver/TraceLogs.html#python)

Additionally, Googling the error message can lead to quick fixes. 

Worst case scenario, you'll have to get into the code itself and fix up whatever is broken.

**The VM is frozen.**

There are probably too many instances of Firefox open. If there are 3 or more instances open, the VM will effectively freeze. Ideally, the script should be the only thing using Firefox.
