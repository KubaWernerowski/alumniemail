
class Person:
    
    def __init__(self, first_name:str, last_name:str,
                 email:str, student_number:str,
                 grad_year:str, preffered_username:str,
                 donor_id: str = "", existing_account:str = "", member_id: str = "") -> None:
    
        self.name = first_name + " " + last_name
        self.email = email
        self.student_number = student_number
        self.grad_year = grad_year
        self.preferred_username = preffered_username
        self.donor_id = donor_id
        self.existing_account = existing_account
        self.member_id = member_id