# Standard library import
from typing import Tuple, List, Dict
from difflib import SequenceMatcher     # For comparing email similarity.
import time
# 3rd party module imports (refer to requirements.txt)
import requests
from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options
# Package imports
import Person


ADMIN_PAGE = "https://adminca.imodules.com/admin/index.aspx?sid=731&gid=1&cid=1318&control_id=1318&gfid=29"
LOGIN_PAGE = "https://secureca.imodules.com/s/731/form-blank/index.aspx?sid=731&gid=1&pgid=3&cid=4"
STUDENT_NUM_PAGE = "https://adminca.imodules.com/admin/index.aspx?sid=731&gid=1&pgid=720&cid=1348&step=2&list_id=32475"


class Webpage:
    
    def __init__(self) -> None:
        opts = Options()
        # Uncomment line below for full log details.
        # opts.log.level = "trace"
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(10)

    def login(self, PAYLOAD:Dict[str, str]) -> None:
        self.driver.get(LOGIN_PAGE)
        username = self.driver.find_element_by_id('cid_40_txtUsername')
        username.send_keys(PAYLOAD['username'])
        password = self.driver.find_element_by_id('cid_40_txtPassword')
        password.send_keys(PAYLOAD['password'])
        login_button = self.driver.find_element_by_id('cid_40_btnLogin')
        login_button.click()
        
    def get_admin_page(self) -> None:
    
        self.driver.get(ADMIN_PAGE)
        
    def get_student_num_page(self) -> None:
        self.driver.get(STUDENT_NUM_PAGE)

    def get_member_record_by_member_id(self, person:Person) -> bool:
        if person.member_id.isdigit():
            self.driver.switch_to_window(self.driver.window_handles[2])       # Focus on member record page
            self.driver.get(f"https://adminca.imodules.com/admin/index.aspx?sid=731&gid=1&cid=1318&control_id=1318&fid=271&gfid=29&mid={person.member_id}")
            # data entry mode
            self.driver.find_element(By.ID, "rg_btnDataEntryMode").click()

            return True
        #TODO: Revisit this and implement it.
        # else:
        #     find_member_record= self.driver.find_element_by_xpath("//div[@class='adminmembersearchpopup']//a").click()
        #     # Select search bar
        #     search_bar = self.driver.find_element_by_xpath("//div[@class='searchfield']//input")
        #     search_bar.send_keys(person.donor_id)
                    
    def member_id_exists(self, person:Person) -> bool:

        student_num_field = self.driver.find_element_by_id("ctl02_mf_52_index0")  
        student_num_field.clear()
        student_num_field.send_keys(person.student_number)
        apply_query = self.driver.find_element_by_xpath("//input[@value='Apply Query']")
        apply_query.click()

        try:
            full_url = self.driver.find_element_by_xpath("//a[contains(@href, 'https://adminca.imodules.com/admin/index.aspx?sid=731&gid=1&cid=1318&mid=')]").get_attribute("href")
        except NoSuchElementException:
            return False

        # member_id is the last part of the URL, 
        # and has an "=" before. Split the string 
        # into pieces around each "=" and return the last one.
        member_id = full_url.split("=")[-1]
        person.member_id = member_id
        return True

    #TODO: update return type to dictionary. Would be easier to work with.
    def validate_request(self, person:Person, reader_row:List) -> Tuple[bool, bool, bool]:
        # VALID AS OF AUGUST 08, 2018
        # Returns a boolean 3-tuple.
        # index 0: whether or not this is a valid request.
        # index 1: whether or not an email has already been created. 
        # index 2: whether or not row was successfully entered with high degree of certainty.
            
        donor_id_field = self.driver.find_element_by_id("rg_gfid_29_fc_7421_TextBox1").get_attribute("value")
        
        grad_year_field = self.driver.find_element_by_id("rg_gfid_29_fc_7423_TextBox1").get_attribute("value")
    
        student_num_field = self.driver.find_element_by_id("rg_gfid_29_fc_7424_TextBox1").get_attribute("value")
    
        alumni_email_created_field = self.driver.find_element_by_id("rg_gfid_29_fc_12573_TextBox1").get_attribute("value")      
        
        alumni_email_requested_cb = self.driver.find_element_by_id("rg_gfid_29_fc_6225_cb")

        TOS_consent_cb = self.driver.find_element_by_id("rg_gfid_29_fc_6119_cb")
        
        alumni_email_prefix_field = self.driver.find_element_by_id("rg_gfid_29_fc_6120_TextBox1")
    
        save_button = self.driver.find_element_by_id("rg_gfid_29_ctl01_btnSave")
    
        # Checks if given student num is neither the person's student number nor their donor id.
        #TODO:Add search by donor id. Would add it as an AND here. Only need one of them to be correct.

        if person.student_number != student_num_field:
            # If not found, put not found.
            reader_row[0] = "NOT FOUND"
            return (False, False, True)
    
        # If email already exists, make note of it in the csv file
        elif alumni_email_created_field != "":
            # Make note that email exists in csv.
            reader_row[0] = f"Email already exists: {alumni_email_created_field}"
            return (False, True, True)

        # Grad years can be inconsistent, so we can make a judgement call
        # By how similar the email prefixes are if the years don't line up
        # in cases where someone puts their masters / PhD grad year.
        elif person.grad_year != grad_year_field and not is_similar_email(person.email, email_address_field.get_attribute("value")):
            reader_row[0] = " " 
            return (False, False, False)
        
        # Update emails.
        try:
            WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.ID, "rg_gfid_29_fc_27866_TextBox1"))
            )
            email_address_field = self.driver.find_element_by_id("rg_gfid_29_fc_27866_TextBox1")
 
            email_confirm_field = self.driver.find_element_by_id("rg_gfid_29_fc_27866_TextBox1_Confirm")
        except Exception:
            print("Couldn't clear emails. Please run the script again.")
            self.driver.quit()
            
        email_address_field.clear()
        email_address_field.send_keys(person.email)
        email_confirm_field.clear()
        email_confirm_field.send_keys(person.email)
        # Click checkboxes if not already checked.
        if not alumni_email_requested_cb.is_selected():
            alumni_email_requested_cb.click()
        if not TOS_consent_cb.is_selected():
            TOS_consent_cb.click()
        # update preferred prefix.
        alumni_email_prefix_field.clear()
        alumni_email_prefix_field.send_keys(person.preferred_username)
        # Save all changes

        save_button.click()

        try:
            placeholder = WebDriverWait(self.driver, 8).until(
                EC.visibility_of_element_located((By.ID, "rg_gfid_29_ctl01_btnSave"))
            )
        except Exception:
            self.driver.quit()
            print("Save failed.")
        # Wait for saving to be complete before moving on.
        
        # Update row with donor id.
        reader_row[0] = donor_id_field

        return (True, False, True)

    def main_validation_process(self, person:Person, reader_row:List) -> bool:
        """
        Returns true iff the row was filled in with high degree of certainty:
        """
        was_filled = False
        # Tries to find member_id
        self.driver.switch_to_window(self.driver.window_handles[1])
        self.member_id_exists(person)
        # Switch to member record page
        self.driver.switch_to_window(self.driver.window_handles[0])
        # Navigate to member record either by donor id or member id.
        #TODO: Add the donor_id search method
        if self.get_member_record_by_member_id(person):
            results = self.validate_request(person, reader_row)
            was_filled = results[2]

        time.sleep(1)

        self.driver.switch_to_window(self.driver.window_handles[1])

        return was_filled

    #TODO: Implement this.
    def find_member_record_by_donor_id(self, person:Person) -> bool:
        # if is_possible_donor_id(validperson.student_number):
        pass

# Misc. helper functions. 
# TODO: Find a way to clean this up and integrate it more nicely.

def is_similar_email(submitted_email:str, iModule_email:str="") -> True:
    """Returns <bool> of whether or not both emails are similar.

    Args:
        submitted_email: email from Excel file.
        iModule_email: email we have on hand, if it exists. Could be empty <str>. (default "")

    Returns:
        True if similarity is >= 0.5, False otherwise.
    """
    if submitted_email == iModule_email: 
        return True
    # Compare email prefixes. Post @ isn't important for determining if it's similar.
    submitted_email_prefix, iModule_email_prefix = submitted_email.split('@')[0], iModule_email.split('@')[0]
    similarity = SequenceMatcher(None, submitted_email_prefix, iModule_email_prefix).ratio()
    # 50% similarity is arbitrary.
    # TODO: Figure out more rigorous way of handling this.
    if similarity >= 0.50:
        return True
    else:
        return False
    

def is_possible_donor_id(num:str) -> bool:
    """Returns <bool> of whether or not a number matches the form of a donor id.
    
    Args:
        num: potentially from the student_number excel column. Should have form 9000dddddd, d is a nonzero digit.
    
    Returns:
        True if <num> matches the correct form that <donor_id> requires.
    """
    return len(num) == 10 and num[:4] == "9000" and num[4] != 0



  